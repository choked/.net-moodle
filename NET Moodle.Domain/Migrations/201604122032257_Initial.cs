namespace NET_Moodle.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Attachments",
                c => new
                    {
                        AttachmentId = c.Int(nullable: false, identity: true),
                        TopicId = c.Int(nullable: false),
                        Content = c.Binary(),
                    })
                .PrimaryKey(t => t.AttachmentId)
                .ForeignKey("dbo.Topics", t => t.TopicId)
                .Index(t => t.TopicId);
            
            CreateTable(
                "dbo.Topics",
                c => new
                    {
                        TopicId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        Restriction = c.Boolean(nullable: false),
                        StartDate = c.DateTime(),
                        CourseId = c.Int(nullable: false),
                        CreationDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.TopicId)
                .ForeignKey("dbo.Courses", t => t.CourseId)
                .Index(t => t.CourseId);
            
            CreateTable(
                "dbo.Courses",
                c => new
                    {
                        CourseId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        CourseType = c.Int(nullable: false),
                        IsPasswordEnable = c.Boolean(nullable: false),
                        Password = c.String(),
                        CreationDate = c.DateTime(nullable: false),
                        StartDate = c.DateTime(nullable: false),
                        IsVisible = c.Boolean(nullable: false),
                        ShortCut = c.String(),
                        ImageId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CourseId)
                .ForeignKey("dbo.Images", t => t.ImageId)
                .Index(t => t.ImageId);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Surname = c.String(),
                        ImageId = c.Int(),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Images", t => t.ImageId)
                .Index(t => t.ImageId)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.Authors",
                c => new
                    {
                        AuthorId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Surname = c.String(),
                        Description = c.String(),
                        Functions = c.String(),
                        ImageId = c.Int(nullable: false),
                        ContactId = c.Int(nullable: false),
                        ApplicationUser_Id = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.AuthorId)
                .ForeignKey("dbo.Contacts", t => t.ContactId)
                .ForeignKey("dbo.Images", t => t.ImageId)
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUser_Id)
                .Index(t => t.ImageId)
                .Index(t => t.ContactId)
                .Index(t => t.ApplicationUser_Id);
            
            CreateTable(
                "dbo.Contacts",
                c => new
                    {
                        ContactId = c.Int(nullable: false, identity: true),
                        EMail = c.String(),
                        Telephone = c.String(),
                        Address = c.String(),
                        Town = c.String(),
                        Country = c.String(),
                    })
                .PrimaryKey(t => t.ContactId);
            
            CreateTable(
                "dbo.Companies",
                c => new
                    {
                        CompanyId = c.Int(nullable: false, identity: true),
                        CompanyName = c.String(),
                        CompanyDescription = c.String(),
                        DateAdded = c.DateTime(nullable: false),
                        ContactId = c.Int(nullable: false),
                        ImageId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CompanyId)
                .ForeignKey("dbo.Contacts", t => t.ContactId)
                .ForeignKey("dbo.Images", t => t.ImageId)
                .Index(t => t.ContactId)
                .Index(t => t.ImageId);
            
            CreateTable(
                "dbo.Images",
                c => new
                    {
                        ImageId = c.Int(nullable: false, identity: true),
                        ImagePath = c.String(),
                        AlternateString = c.String(),
                    })
                .PrimaryKey(t => t.ImageId);
            
            CreateTable(
                "dbo.Lecturers",
                c => new
                    {
                        LecturerId = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        HomePageAddress = c.String(),
                        ContactId = c.Int(nullable: false),
                        ImageId = c.Int(nullable: false),
                        ApplicationUser_Id = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.LecturerId)
                .ForeignKey("dbo.Contacts", t => t.ContactId)
                .ForeignKey("dbo.Images", t => t.ImageId)
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUser_Id)
                .Index(t => t.ContactId)
                .Index(t => t.ImageId)
                .Index(t => t.ApplicationUser_Id);
            
            CreateTable(
                "dbo.News",
                c => new
                    {
                        NewsId = c.Int(nullable: false, identity: true),
                        TopicName = c.String(),
                        PhotoName = c.String(),
                        Description = c.String(),
                        CreationDate = c.DateTime(nullable: false),
                        ImageId = c.Int(nullable: false),
                        LecturerId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.NewsId)
                .ForeignKey("dbo.Images", t => t.ImageId)
                .ForeignKey("dbo.Lecturers", t => t.LecturerId)
                .Index(t => t.ImageId)
                .Index(t => t.LecturerId);
            
            CreateTable(
                "dbo.Tags",
                c => new
                    {
                        TagId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        Lecturer_LecturerId = c.Int(),
                    })
                .PrimaryKey(t => t.TagId)
                .ForeignKey("dbo.Lecturers", t => t.Lecturer_LecturerId)
                .Index(t => t.Lecturer_LecturerId);
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.CourseLecturerAssigment",
                c => new
                    {
                        LecturerId = c.Int(nullable: false),
                        CourseId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.LecturerId, t.CourseId })
                .ForeignKey("dbo.Lecturers", t => t.LecturerId, cascadeDelete: true)
                .ForeignKey("dbo.Courses", t => t.CourseId, cascadeDelete: true)
                .Index(t => t.LecturerId)
                .Index(t => t.CourseId);
            
            CreateTable(
                "dbo.NewsTag",
                c => new
                    {
                        NewsId = c.Int(nullable: false),
                        TagId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.NewsId, t.TagId })
                .ForeignKey("dbo.News", t => t.NewsId, cascadeDelete: true)
                .ForeignKey("dbo.Tags", t => t.TagId, cascadeDelete: true)
                .Index(t => t.NewsId)
                .Index(t => t.TagId);
            
            CreateTable(
                "dbo.CompanyTag",
                c => new
                    {
                        CompanyId = c.Int(nullable: false),
                        TagId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.CompanyId, t.TagId })
                .ForeignKey("dbo.Companies", t => t.CompanyId, cascadeDelete: true)
                .ForeignKey("dbo.Tags", t => t.TagId, cascadeDelete: true)
                .Index(t => t.CompanyId)
                .Index(t => t.TagId);
            
            CreateTable(
                "dbo.CourseUsers",
                c => new
                    {
                        AspNetUsersId = c.String(name: "AspNetUsers.Id", nullable: false, maxLength: 128),
                        CourseId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.AspNetUsersId, t.CourseId })
                .ForeignKey("dbo.AspNetUsers", t => t.AspNetUsersId, cascadeDelete: true)
                .ForeignKey("dbo.Courses", t => t.CourseId, cascadeDelete: true)
                .Index(t => t.AspNetUsersId)
                .Index(t => t.CourseId);
            
            CreateTable(
                "dbo.CourseTag",
                c => new
                    {
                        CourseId = c.Int(nullable: false),
                        TagId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.CourseId, t.TagId })
                .ForeignKey("dbo.Courses", t => t.CourseId, cascadeDelete: true)
                .ForeignKey("dbo.Tags", t => t.TagId, cascadeDelete: true)
                .Index(t => t.CourseId)
                .Index(t => t.TagId);
            
            CreateTable(
                "dbo.TopicTag",
                c => new
                    {
                        TopicId = c.Int(nullable: false),
                        TagId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.TopicId, t.TagId })
                .ForeignKey("dbo.Topics", t => t.TopicId, cascadeDelete: true)
                .ForeignKey("dbo.Tags", t => t.TagId, cascadeDelete: true)
                .Index(t => t.TopicId)
                .Index(t => t.TagId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.Attachments", "TopicId", "dbo.Topics");
            DropForeignKey("dbo.TopicTag", "TagId", "dbo.Tags");
            DropForeignKey("dbo.TopicTag", "TopicId", "dbo.Topics");
            DropForeignKey("dbo.Topics", "CourseId", "dbo.Courses");
            DropForeignKey("dbo.CourseTag", "TagId", "dbo.Tags");
            DropForeignKey("dbo.CourseTag", "CourseId", "dbo.Courses");
            DropForeignKey("dbo.Courses", "ImageId", "dbo.Images");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Lecturers", "ApplicationUser_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUsers", "ImageId", "dbo.Images");
            DropForeignKey("dbo.CourseUsers", "CourseId", "dbo.Courses");
            DropForeignKey("dbo.CourseUsers", "AspNetUsers.Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Authors", "ApplicationUser_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Authors", "ImageId", "dbo.Images");
            DropForeignKey("dbo.Authors", "ContactId", "dbo.Contacts");
            DropForeignKey("dbo.CompanyTag", "TagId", "dbo.Tags");
            DropForeignKey("dbo.CompanyTag", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.Companies", "ImageId", "dbo.Images");
            DropForeignKey("dbo.Tags", "Lecturer_LecturerId", "dbo.Lecturers");
            DropForeignKey("dbo.NewsTag", "TagId", "dbo.Tags");
            DropForeignKey("dbo.NewsTag", "NewsId", "dbo.News");
            DropForeignKey("dbo.News", "LecturerId", "dbo.Lecturers");
            DropForeignKey("dbo.News", "ImageId", "dbo.Images");
            DropForeignKey("dbo.Lecturers", "ImageId", "dbo.Images");
            DropForeignKey("dbo.CourseLecturerAssigment", "CourseId", "dbo.Courses");
            DropForeignKey("dbo.CourseLecturerAssigment", "LecturerId", "dbo.Lecturers");
            DropForeignKey("dbo.Lecturers", "ContactId", "dbo.Contacts");
            DropForeignKey("dbo.Companies", "ContactId", "dbo.Contacts");
            DropIndex("dbo.TopicTag", new[] { "TagId" });
            DropIndex("dbo.TopicTag", new[] { "TopicId" });
            DropIndex("dbo.CourseTag", new[] { "TagId" });
            DropIndex("dbo.CourseTag", new[] { "CourseId" });
            DropIndex("dbo.CourseUsers", new[] { "CourseId" });
            DropIndex("dbo.CourseUsers", new[] { "AspNetUsers.Id" });
            DropIndex("dbo.CompanyTag", new[] { "TagId" });
            DropIndex("dbo.CompanyTag", new[] { "CompanyId" });
            DropIndex("dbo.NewsTag", new[] { "TagId" });
            DropIndex("dbo.NewsTag", new[] { "NewsId" });
            DropIndex("dbo.CourseLecturerAssigment", new[] { "CourseId" });
            DropIndex("dbo.CourseLecturerAssigment", new[] { "LecturerId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.Tags", new[] { "Lecturer_LecturerId" });
            DropIndex("dbo.News", new[] { "LecturerId" });
            DropIndex("dbo.News", new[] { "ImageId" });
            DropIndex("dbo.Lecturers", new[] { "ApplicationUser_Id" });
            DropIndex("dbo.Lecturers", new[] { "ImageId" });
            DropIndex("dbo.Lecturers", new[] { "ContactId" });
            DropIndex("dbo.Companies", new[] { "ImageId" });
            DropIndex("dbo.Companies", new[] { "ContactId" });
            DropIndex("dbo.Authors", new[] { "ApplicationUser_Id" });
            DropIndex("dbo.Authors", new[] { "ContactId" });
            DropIndex("dbo.Authors", new[] { "ImageId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.AspNetUsers", new[] { "ImageId" });
            DropIndex("dbo.Courses", new[] { "ImageId" });
            DropIndex("dbo.Topics", new[] { "CourseId" });
            DropIndex("dbo.Attachments", new[] { "TopicId" });
            DropTable("dbo.TopicTag");
            DropTable("dbo.CourseTag");
            DropTable("dbo.CourseUsers");
            DropTable("dbo.CompanyTag");
            DropTable("dbo.NewsTag");
            DropTable("dbo.CourseLecturerAssigment");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.Tags");
            DropTable("dbo.News");
            DropTable("dbo.Lecturers");
            DropTable("dbo.Images");
            DropTable("dbo.Companies");
            DropTable("dbo.Contacts");
            DropTable("dbo.Authors");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.Courses");
            DropTable("dbo.Topics");
            DropTable("dbo.Attachments");
        }
    }
}
