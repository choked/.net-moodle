﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace NET_Moodle.Domain.Helpers
{
    public class Crypto
    {
        public static string EncryptPassword(string password)
        {
            byte[] hash;
            using (MD5 md5 = MD5.Create())
            {
                hash = md5.ComputeHash(Encoding.UTF8.GetBytes(password));
            }
            return Encoding.UTF8.GetString(hash);
        }
    }
}
