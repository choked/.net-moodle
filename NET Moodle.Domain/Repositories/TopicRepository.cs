﻿using NET_Moodle.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NET_Moodle.Domain.Model;

namespace NET_Moodle.Domain.Repositories
{
    public class TopicRepository : ITopic
    {
        private MoodleDbContext dbContext;

        public TopicRepository(IDbContext dbContext)
        {
            this.dbContext = (MoodleDbContext)dbContext;
        }

        public IEnumerable<Topic> GetCourseTopics(int courseId)
        {
            return dbContext.Topics.Where(n => n.CourseId == courseId);
        }

        public void Add(Topic item)
        {
            dbContext.Topics.Add(item);
            dbContext.SaveChanges();
        }

        public void Delete(Topic entry)
        {
            var content = new Topic() { TopicId = entry.TopicId };
            dbContext.Topics.Attach(content);
            dbContext.Topics.Remove(content);
            dbContext.SaveChanges();

        }
        
        public Topic GetElementById(int id)
        {
            return dbContext.Topics.FirstOrDefault(n => n.TopicId == id);
        }

        public void Update(Topic entry)
        {
            var Topic = dbContext.Topics.FirstOrDefault(n => n.TopicId == entry.TopicId);
            if (Topic == null)
            {
                throw new ArgumentException("You cannot update not existing row");
            }
            Topic = entry;
            dbContext.SaveChanges();
        }

        public void Dispose()
        {
            if (dbContext != null)
            {
                dbContext.Dispose();
            }
        }

        public IEnumerable<Topic> Get()
        {
            return dbContext.Topics.AsEnumerable();
        }
    }
}
