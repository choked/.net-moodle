﻿using NET_Moodle.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NET_Moodle.Domain.Model;
using NET_Moodle.Domain.Helpers;

namespace NET_Moodle.Domain.Repositories
{
    public class CourseRepository : ICourse
    {
        private MoodleDbContext _dbContext;

        public CourseRepository(IDbContext dbContext)
        {
            _dbContext = (MoodleDbContext)dbContext;
        }

        public void DeleteById(int id)
        {
            var course = _dbContext.Courses.FirstOrDefault(n => n.CourseId == id);
            if (course == null)
            {
                throw new Exception("Course not exists");
            }
            //delete topics
            var topics = _dbContext.Topics.Where(n => n.CourseId == id).AsEnumerable();
            _dbContext.Topics.RemoveRange(topics);

            _dbContext.Courses.Remove(course);
            _dbContext.SaveChanges();
        }
        public void Add(Course item)
        {
            string hashPassword = Crypto.EncryptPassword(item.Password);
            item.Password = hashPassword;
            _dbContext.Courses.Add(item);
            _dbContext.SaveChanges();
        }

        public void Delete(Course entry)
        {
            var content = new Course() { CourseId = entry.CourseId };
            _dbContext.Courses.Attach(content);
            _dbContext.Courses.Remove(content);
            _dbContext.SaveChanges();

        }

        public Course GetElementById(int id)
        {
            return _dbContext.Courses.FirstOrDefault(n => n.CourseId == id);
        }
        public void UpdateDescription(Course entry)
        {
            var course = _dbContext.Courses.FirstOrDefault(n => n.CourseId == entry.CourseId);
            if (course == null)
            {
                throw new ArgumentException("You cannot update not existing row");
            }
            course.CourseType = entry.CourseType;
            course.IsVisible = entry.IsVisible;
            course.IsPasswordEnable = entry.IsPasswordEnable;
            if (entry.IsPasswordEnable)
            {
                course.Password = Crypto.EncryptPassword(entry.Password);
            }
            course.ShortCut = entry.ShortCut;
            _dbContext.SaveChanges();
        }

        public void Update(Course entry)
        {
            var course = _dbContext.Courses.FirstOrDefault(n => n.CourseId == entry.CourseId);
            if (course == null)
            {
                throw new ArgumentException("You cannot update not existing row");
            }
            course.Name = entry.Name;
            course.ShortCut = entry.ShortCut;
            course.Description = entry.Description;
            _dbContext.SaveChanges();
        }
        public void AssignUserToCourse(int courseId, string userId, string password)
        {
            var course = _dbContext.Courses.FirstOrDefault(n => n.CourseId == courseId);
            if (course == null || (course.IsPasswordEnable && Crypto.EncryptPassword(password) != course.Password) || string.IsNullOrEmpty(userId))
            {
                throw new ArgumentException("Bad arguments passed to function");
            }
            try
            {
                var user = _dbContext.Users.FirstOrDefault(n => n.Id == userId);
                _dbContext.Users.FirstOrDefault(n => n.Id == userId)?
                    .Courses.Add(course);
                _dbContext.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        public void Dispose()
        {
            if (_dbContext != null)
            {
                _dbContext.Dispose();
            }
        }

        public IEnumerable<Course> Get()
        {
            return _dbContext.Courses.AsEnumerable();
        }

        public IEnumerable<Course> GetUserCourses(string userId)
        {
            var result = _dbContext.Users
                    .FirstOrDefault(n => n.Id == userId)
                    .Courses;
            return result;
        }

        public IEnumerable<ApplicationUser> GetCourseUsers(int courseId)
        {
            var result = _dbContext.Courses
                    .FirstOrDefault(n => n.CourseId == courseId)
                    .ApplicationUsers;
            return result;
        }
    }
}