﻿using NET_Moodle.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NET_Moodle.Domain.Model;

namespace NET_Moodle.Domain.Repositories
{
    public class NewsRepository : INews
    {
        private MoodleDbContext dbContext;

        public NewsRepository(IDbContext dbContext)
        {
            this.dbContext = (MoodleDbContext)dbContext;
        }
        public void DeleteById(int newsId)
        {
            var news = dbContext.News.FirstOrDefault(n => n.NewsId == newsId);
            if (news == null)
            {
                throw new Exception("News does not exists");
            }
            dbContext.News.Remove(news);
            dbContext.SaveChanges();

        }

        public void Add(News item)
        {
            dbContext.News.Add(item);
            dbContext.SaveChanges();
        }

        public void Delete(News entry)
        {
            var content = new News() { NewsId = entry.NewsId };
            dbContext.News.Attach(content);
            dbContext.News.Remove(content);
            dbContext.SaveChanges();
        }

        public News GetElementById(int id)
        {
            return dbContext.News.FirstOrDefault(n => n.NewsId == id);
        }

        public void Update(News entry)
        {
            var News = dbContext.News.FirstOrDefault(n => n.NewsId == entry.NewsId);
            if (News == null)
            {
                throw new ArgumentException("You cannot update not existing row");
            }
            News = entry;
            dbContext.SaveChanges();
        }

        public void Dispose()
        {
            if (dbContext != null)
            {
                dbContext.Dispose();
            }
        }

        public IEnumerable<News> Get()
        {
            return dbContext.News;
        }
    }
}
