﻿using NET_Moodle.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NET_Moodle.Domain.Model;

namespace NET_Moodle.Domain.Repositories
{
    public class AttachmentRepository : IAttachment
    {
        private MoodleDbContext dbContext;

        public AttachmentRepository(IDbContext dbContext)
        {
            this.dbContext = (MoodleDbContext)dbContext;
        }

        public void Add(Attachment item)
        {
            dbContext.Attachments.Add(item);
            dbContext.SaveChanges();
        }

        public void Delete(Attachment entry)
        {
            var content = new Attachment() { AttachmentId = entry.AttachmentId };
            dbContext.Attachments.Attach(content);
            dbContext.Attachments.Remove(content);
            dbContext.SaveChanges();

        }

        public Attachment GetElementById(int id)
        {
            return dbContext.Attachments.FirstOrDefault(n => n.AttachmentId == id);
        }

        public void Update(Attachment entry)
        {
            var Attachment = dbContext.Attachments.FirstOrDefault(n => n.AttachmentId == entry.AttachmentId);
            if (Attachment == null)
            {
                throw new ArgumentException("You cannot update not existing row");
            }
            Attachment = entry;
            dbContext.SaveChanges();
        }

        public void Dispose()
        {
            if (dbContext != null)
            {
                dbContext.Dispose();
            }
        }

        public IEnumerable<Attachment> Get()
        {
            return dbContext.Attachments.AsEnumerable();
        }
    }
}
