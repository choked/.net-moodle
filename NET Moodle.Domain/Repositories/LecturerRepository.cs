﻿using NET_Moodle.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NET_Moodle.Domain.Model;

namespace NET_Moodle.Domain.Repositories
{
    public class LecturerRepository : ILecturer
    {
        private MoodleDbContext dbContext;

        public LecturerRepository(IDbContext dbContext)
        {
            this.dbContext = (MoodleDbContext)dbContext;
        }

        public void Add(Lecturer item)
        {
            dbContext.Lecturers.Add(item);
            dbContext.SaveChanges();
        }

        public void Delete(Lecturer entry)
        {
            var content = new Lecturer() { LecturerId = entry.LecturerId };
            dbContext.Lecturers.Attach(content);
            dbContext.Lecturers.Remove(content);
            dbContext.SaveChanges();

        }

        public Lecturer GetElementById(int id)
        {
            return dbContext.Lecturers.FirstOrDefault(n => n.LecturerId == id);
        }

        public void Update(Lecturer entry)
        {
            var Lecturer = dbContext.Lecturers.FirstOrDefault(n => n.LecturerId == entry.LecturerId);
            if (Lecturer == null)
            {
                throw new ArgumentException("You cannot update not existing row");
            }
            Lecturer = entry;
            dbContext.SaveChanges();
        }

        public void Dispose()
        {
            if (dbContext != null)
            {
                dbContext.Dispose();
            }
        }
        public Lecturer GetByUserId(string userId)
        {
            return dbContext.Lecturers.FirstOrDefault(n => n.ApplicationUser.Id == userId);
        }

        public IEnumerable<Lecturer> Get()
        {
            return dbContext.Lecturers.AsEnumerable();
        }
    }
}
