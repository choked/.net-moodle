﻿using NET_Moodle.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NET_Moodle.Domain.Model;

namespace NET_Moodle.Domain.Repositories
{
    public class CompanyRepository : ICompany
    {
        private MoodleDbContext dbContext;

        public CompanyRepository(IDbContext dbContext)
        {
            this.dbContext = (MoodleDbContext)dbContext;
        }

        public void Add(Company item)
        {
            dbContext.Companies.Add(item);
            dbContext.SaveChanges();
        }

        public void Delete(Company entry)
        {
            var content = new Company() { CompanyId = entry.CompanyId};
            dbContext.Companies.Attach(content);
            dbContext.Companies.Remove(content);
            dbContext.SaveChanges();

        }
        public Company GetElementById(int id)
        {
            return dbContext.Companies.FirstOrDefault(n => n.CompanyId == id);
        }

        public void Update(Company entry)
        {
            var company = dbContext.Companies.FirstOrDefault(n => n.CompanyId == entry.CompanyId);
            if (company == null)
            {
                throw new ArgumentException("You cannot update not existing row");
            }
            company = entry;
            dbContext.SaveChanges();
        }

        public void Dispose()
        {
            if (dbContext != null)
            {
                dbContext.Dispose();
            }
        }

        public IEnumerable<Company> Get()
        {
            return dbContext.Companies.AsEnumerable();
        }
    }
}
