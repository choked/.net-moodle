﻿using NET_Moodle.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NET_Moodle.Domain.Model;

namespace NET_Moodle.Domain.Repositories
{
    public class ImageRepository : IImage
    {
        private MoodleDbContext dbContext;

        public ImageRepository(IDbContext dbContext)
        {
            this.dbContext = (MoodleDbContext)dbContext;
        }

        public void Add(Image item)
        {
            dbContext.Images.Add(item);
            dbContext.SaveChanges();
        }

        public void Delete(Image entry)
        {
            var content = new Image() { ImageId = entry.ImageId };
            dbContext.Images.Attach(content);
            dbContext.Images.Remove(content);
            dbContext.SaveChanges();

        }

        public Image GetElementById(int id)
        {
            return dbContext.Images.FirstOrDefault(n => n.ImageId == id);
        }

        public void Update(Image entry)
        {
            var Image = dbContext.Images.FirstOrDefault(n => n.ImageId == entry.ImageId);
            if (Image == null)
            {
                throw new ArgumentException("You cannot update not existing row");
            }
            Image = entry;
            dbContext.SaveChanges();
        }

        public void Dispose()
        {
            if (dbContext != null)
            {
                dbContext.Dispose();
            }
        }

        public IEnumerable<Image> Get()
        {
            return dbContext.Images.AsEnumerable();
        }
    }
}
