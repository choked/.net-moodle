﻿using NET_Moodle.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NET_Moodle.Domain.Model;

namespace NET_Moodle.Domain.Repositories
{
    public class TagRepository : ITag
    {
        private MoodleDbContext dbContext;

        public TagRepository(IDbContext dbContext)
        {
            this.dbContext = (MoodleDbContext)dbContext;
        }

        public void Add(Tag item)
        {
            dbContext.Tags.Add(item);
            dbContext.SaveChanges();
        }

        public void Delete(Tag entry)
        {
            var content = new Tag() { TagId = entry.TagId };
            dbContext.Tags.Attach(content);
            dbContext.Tags.Remove(content);
            dbContext.SaveChanges();

        }

        
        public Tag GetElementById(int id)
        {
            return dbContext.Tags.FirstOrDefault(n => n.TagId == id);
        }

        public void Update(Tag entry)
        {
            var Tag = dbContext.Tags.FirstOrDefault(n => n.TagId == entry.TagId);
            if (Tag == null)
            {
                throw new ArgumentException("You cannot update not existing row");
            }
            Tag = entry;
            dbContext.SaveChanges();
        }

        public void Dispose()
        {
            if (dbContext != null)
            {
                dbContext.Dispose();
            }
        }

        public IEnumerable<Tag> Get()
        {
            return dbContext.Tags.AsEnumerable();
        }
    }
}
