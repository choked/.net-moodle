﻿using NET_Moodle.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NET_Moodle.Domain.Model;

namespace NET_Moodle.Domain.Repositories
{
    public class UserRepository : IUserRepository
    {
        private MoodleDbContext dbContext;

        public UserRepository(IDbContext dbContext)
        {
            this.dbContext = (MoodleDbContext)dbContext;
        }
        public IEnumerable<ApplicationUser> GetUsers(bool includeAdministration=false)
        {
            return dbContext.Users;
        }
    }
}
