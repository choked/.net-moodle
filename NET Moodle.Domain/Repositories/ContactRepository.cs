﻿using NET_Moodle.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NET_Moodle.Domain.Model;

namespace NET_Moodle.Domain.Repositories
{
    public class ContactRepository : IContact
    {
        private MoodleDbContext dbContext;

        public ContactRepository(IDbContext dbContext)
        {
            this.dbContext = (MoodleDbContext)dbContext;
        }

        public void Add(Contact item)
        {
            dbContext.Contacts.Add(item);
            dbContext.SaveChanges();
        }

        public void Delete(Contact entry)
        {
            var content = new Contact() { ContactId = entry.ContactId };
            dbContext.Contacts.Attach(content);
            dbContext.Contacts.Remove(content);
            dbContext.SaveChanges();

        }

        public Contact GetElementById(int id)
        {
            return dbContext.Contacts.FirstOrDefault(n => n.ContactId == id);
        }

        public void Update(Contact entry)
        {
            var Contact = dbContext.Contacts.FirstOrDefault(n => n.ContactId == entry.ContactId);
            if (Contact == null)
            {
                throw new ArgumentException("You cannot update not existing row");
            }
            Contact = entry;
            dbContext.SaveChanges();
        }

        public void Dispose()
        {
            if (dbContext != null)
            {
                dbContext.Dispose();
            }
        }

        public IEnumerable<Contact> Get()
        {
            return dbContext.Contacts.AsEnumerable();
        }
    }
}
