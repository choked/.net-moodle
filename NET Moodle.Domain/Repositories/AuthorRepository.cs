﻿using NET_Moodle.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NET_Moodle.Domain.Model;

namespace NET_Moodle.Domain.Repositories
{
    public class AuthorRepository : IAuthor
    {
        private MoodleDbContext dbContext;

        public AuthorRepository(IDbContext dbContext)
        {
            this.dbContext = (MoodleDbContext)dbContext;
        }
        public void Add(Author item)
        {
            dbContext.Authors.Add(item);
        }

        public void Delete(Author item)
        {
            //dbContext.Authors.Remove()
        }

        public IEnumerable<Author> Get()
        {
            return dbContext.Authors;
        }

        public Author GetElementById(int id)
        {
            return dbContext.Authors.FirstOrDefault(n => n.AuthorId == id);
        }

        public void Update(Author item)
        {
            throw new NotImplementedException();
        }
    }
}
