﻿using Microsoft.AspNet.Identity.EntityFramework;
using NET_Moodle.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NET_Moodle.Domain.Model
{
    public class MoodleDbContext : IdentityDbContext<ApplicationUser>, IDbContext
    {
        public MoodleDbContext()
            : base("MoodleDb", throwIfV1Schema: false)
        {
        }
        public DbSet<Lecturer> Lecturers { get; set; }
        public DbSet<Author> Authors { get; set; }
        public DbSet<Course> Courses { get; set; }
        public DbSet<Topic> Topics { get; set; }
        public DbSet<Attachment> Attachments { get; set; }

        public DbSet<Company> Companies { get; set; }
        public DbSet<Contact> Contacts { get; set; }

        public DbSet<Image> Images { get; set; }
        public DbSet<News> News { get; set; }
        public DbSet<Tag> Tags { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
           
            modelBuilder.Entity<ApplicationUser>()
            .HasOptional(user => user.Lecturer)
            .WithRequired(lecturer => lecturer.ApplicationUser);

            modelBuilder.Entity<ApplicationUser>()
           .HasOptional(user => user.Author)
           .WithRequired(author => author.ApplicationUser);


            //many-to-many
            modelBuilder.Entity<News>()
                .HasMany<Tag>(s => s.Tags)
                .WithMany(c => c.News)
                .Map(cs =>
                {
                    cs.MapLeftKey("NewsId");
                    cs.MapRightKey("TagId");
                    cs.ToTable("NewsTag");
                });

            modelBuilder.Entity<Topic>()
                .HasMany<Tag>(s => s.Tags)
                .WithMany(c => c.Topics)
                .Map(cs =>
                {
                    cs.MapLeftKey("TopicId");
                    cs.MapRightKey("TagId");
                    cs.ToTable("TopicTag");
                });

            modelBuilder.Entity<Company>()
            .HasMany<Tag>(s => s.Tags)
            .WithMany(c => c.Companies)
            .Map(cs =>
            {
                cs.MapLeftKey("CompanyId");
                cs.MapRightKey("TagId");
                cs.ToTable("CompanyTag");
            });

            modelBuilder.Entity<Course>()
           .HasMany<Tag>(s => s.Tags)
           .WithMany(c => c.Courses)
           .Map(cs =>
           {
               cs.MapLeftKey("CourseId");
               cs.MapRightKey("TagId");
               cs.ToTable("CourseTag");
           });

            modelBuilder.Entity<ApplicationUser>()
           .HasMany<Course>(s => s.Courses)
           .WithMany(c => c.ApplicationUsers)
           .Map(cs =>
           {
               cs.MapLeftKey("AspNetUsers.Id");
               cs.MapRightKey("CourseId");
               cs.ToTable("CourseUsers");
           });

            modelBuilder.Entity<Lecturer>()
           .HasMany<Course>(s => s.Courses)
           .WithMany(c => c.Lecturers)
           .Map(cs =>
           {
               cs.MapLeftKey("LecturerId");
               cs.MapRightKey("CourseId");
               cs.ToTable("CourseLecturerAssigment");
           });

        }

        public static MoodleDbContext Create()
        {
            return new MoodleDbContext();
        }
    }

}
