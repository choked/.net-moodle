﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NET_Moodle.Domain.Model
{
    public class Course
    {
        public Course()
        {

        }
        [Key]
        public int CourseId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public CourseTypeEnum CourseType { get; set; }
        public bool IsPasswordEnable { get; set; }
        public string Password { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime StartDate { get; set; }
        public bool IsVisible { get; set; }
        public string ShortCut { get; set; }
        [ForeignKey("Image")]
        public int ImageId { get; set; }

        public virtual ICollection<Lecturer> Lecturers { get; set; }
        public virtual ICollection<Topic> Topics { get; set; }
        public virtual ICollection<ApplicationUser> ApplicationUsers { get; set; }
        public virtual Image Image { get; set; }
        public virtual ICollection<Tag> Tags { get; set; }

    }
}
