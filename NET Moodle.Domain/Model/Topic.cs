﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NET_Moodle.Domain.Model
{
    public class Topic
    {
        public Topic()
        {

        }
        [Key]
        public int TopicId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool Restriction { get; set; }
        public DateTime? StartDate { get; set; }
        [ForeignKey("Course")]
        public int CourseId { get; set; }
        public DateTime CreationDate { get; set; }


        public virtual Course Course { get; set; }
        public virtual ICollection<Attachment> Attachments { get; set; }
        public virtual ICollection<Tag> Tags { get; set; }


    }
}
