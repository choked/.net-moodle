﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NET_Moodle.Domain.Model
{
    public class Attachment
    {
        public Attachment()
        {

        }
        [Key]
        public int AttachmentId { get; set; }
        [ForeignKey("Topic")]
        public int TopicId { get; set; }
        public byte[] Content { get; set; }

        public virtual Topic Topic { get; set; }

    }
}
