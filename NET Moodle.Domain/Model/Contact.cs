﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NET_Moodle.Domain.Model
{
    public class Contact
    {
        public Contact() { }
        [Key]
        public int ContactId { get; set; }
        public string EMail { get; set; }
        public string Telephone { get; set; }
        public string Address { get; set; }
        public string Town { get; set; }
        public string Country { get; set; }

        public virtual ICollection<Lecturer> Lecturers { get; set; }
        public virtual ICollection<Company> Companies { get; set; }
        public virtual ICollection<Author> Authors { get; set; }

    }
}