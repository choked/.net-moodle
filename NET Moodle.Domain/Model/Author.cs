﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NET_Moodle.Domain.Model
{
    public class Author
    {
        [Key]
        public int AuthorId { get; set; }

        public string Name { get; set; }
        public string Surname { get; set; }
        public string Description { get; set; }
        public string Functions { get; set; }
        [ForeignKey("Image")]
        public int ImageId { get; set; }
        [ForeignKey("Contact")]
        public int ContactId { get; set; }

        public virtual Contact Contact { get; set; }
        public virtual Image Image { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }
    }
}
