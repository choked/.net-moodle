﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NET_Moodle.Domain.Model
{
    public class Company
    {
        public Company()
        {

        }
        [Key]
        public int CompanyId { get; set; }
        public string CompanyName { get; set; }
        public string CompanyDescription { get; set; }
        public DateTime DateAdded { get; set; }
        [ForeignKey("Contact")]
        public int ContactId { get; set; }
        [ForeignKey("Image")]
        public int ImageId { get; set; }

        public virtual Contact Contact { get; set; }
        public virtual ICollection<Tag> Tags { get; set; }
        public virtual Image Image { get; set; }

    }
}
