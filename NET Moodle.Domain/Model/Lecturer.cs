﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NET_Moodle.Domain.Model
{
    public class Lecturer
    {
        public Lecturer()
        {

        }
        [Key]
        public int LecturerId { get; set; }
        public string Description { get; set; }
        public string HomePageAddress { get; set; }

        [ForeignKey("Contact")]
        public int ContactId { get; set; }
        [ForeignKey("Image")]
        public int ImageId { get; set; }

        public virtual Image Image { get; set; }
        public virtual Contact Contact { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }

        public virtual ICollection<Tag> Tags { get; set; }
        public virtual ICollection<Course> Courses { get; set; }
        public virtual ICollection<News> News { get; set; }

    }
}
