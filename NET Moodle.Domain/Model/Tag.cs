﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NET_Moodle.Domain.Model
{
    public class Tag
    {
        public Tag()
        {

        }
        [Key]
        public int TagId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<News> News { get; set; }
        public virtual ICollection<Company> Companies { get; set; }
        public virtual ICollection<Course> Courses { get; set; }
        public virtual ICollection<Topic> Topics { get; set; }

    }
}
