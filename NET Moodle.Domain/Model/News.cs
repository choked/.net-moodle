﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NET_Moodle.Domain.Model
{
    public class News
    {
        public News()
        {

        }
        [Key]
        public int NewsId { get; set; }
        public string TopicName { get; set; }
        public string PhotoName { get; set; }
        public string Description { get; set; }
        public DateTime CreationDate { get; set; }
        [ForeignKey("Image")]
        public int ImageId { get; set; }
        [ForeignKey("Lecturer")]
        public int LecturerId { get; set; }


        public virtual Image Image { get; set; }
        public virtual Lecturer Lecturer { get; set; }


        public virtual ICollection<Tag> Tags { get; set; }

    }
}
