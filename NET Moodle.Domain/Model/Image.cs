﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NET_Moodle.Domain.Model
{
    public class Image
    {
        public Image()
        {

        }
        [Key]
        public int ImageId { get; set; }
        public string ImagePath { get; set; }
        public string AlternateString { get; set; }

        public virtual ICollection<Company> Companies { get; set; }
        public virtual ICollection<Course> Courses { get; set; }
        public virtual ICollection<Lecturer> Lecturers { get; set; }
        public virtual ICollection<News> News { get; set; }
        public virtual ICollection<Author> Authors { get; set; }


    }
}
