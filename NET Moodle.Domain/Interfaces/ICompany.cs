﻿using NET_Moodle.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NET_Moodle.Domain.Interfaces
{
    public interface ICompany : IRepository<Company>
    {
        //IEnumerable<Company> GetCompanies();
    }
}
