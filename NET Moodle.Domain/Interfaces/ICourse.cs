﻿using NET_Moodle.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NET_Moodle.Domain.Interfaces
{
    public interface ICourse : IRepository<Course>
    {
        //IEnumerable<Course> GetCourses();
        void DeleteById(int id);
        IEnumerable<Course> GetUserCourses(string userId);
        void AssignUserToCourse(int courseId, string userId, string password);
        IEnumerable<ApplicationUser> GetCourseUsers(int courseId);
        void UpdateDescription(Course entry);
    }
}
