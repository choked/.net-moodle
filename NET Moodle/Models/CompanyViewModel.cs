﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NET_Moodle.Models
{
    public class CompanyViewModel
    {
        [Display(Name="Nazwa firmy")]
        [Required]
        public string Name { get; set; }
        [Display(Name="Opis")]
        [Required]
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public string AltImage { get; set; }
        [EmailAddress(ErrorMessage ="Podaj adres e-mail")]
        [Display(Name="E-mail")]
        public string EMail { get; set; }
        [Display(Name="Adres")]
        public string Address { get; set; }
        [DataType(DataType.PhoneNumber,ErrorMessage ="Podaj numer telefonu")]
        [Display(Name="Numer telefonu")]
        [Required]
        public string Telephone { get; set; }
    }
}