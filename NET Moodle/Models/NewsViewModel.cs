﻿using NET_Moodle.Domain.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NET_Moodle.Models
{
    public class NewsViewModel
    {
        [Display(AutoGenerateField = false)]
        public int NewsId { get; set; }

        [Display(Name = "Tytuł")]
        [Required(ErrorMessage = "Podaj tytył newsa")]
        public string Title { get; set; }

        [Display(Name = "Opis")]
        [Required(ErrorMessage = "Podaj opis newsa")]
        public string Description { get; set; }

        [Display(Name = "Data")]
        [Required(ErrorMessage = "Podaj date dodania")]
        public DateTime CreationDate { get; set; }

        [Display(AutoGenerateField = false)]
        public string ImagePath { get; set; }

        [Display(Name = "Powiązane")]
        [Required(ErrorMessage = "Podaj id prelegenta")]
        public int LecturerId { get; set; }

        public static NewsViewModel ToNewsViewModel(News news)
        {

            return new NewsViewModel()
            {
                Title = news.TopicName,
                Description = news.Description,
                CreationDate = news.CreationDate,
                ImagePath = news.Image.ImagePath,
                LecturerId = news.LecturerId,
                NewsId = news.NewsId
            };
        }
    }
}