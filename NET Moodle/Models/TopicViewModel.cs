﻿using NET_Moodle.Domain.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NET_Moodle.Models
{
    public class TopicViewModel
    {
        [Display(AutoGenerateField = false)]
        public int Id { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Nazwa", Description = "Nazwa tematu")]
        public string Name { get; set; }

        [Required]
        [DataType(DataType.MultilineText)]
        [Display(Name = "Opis", Description = "Opis tematu")]
        public string Description { get; set; }

        [Required]
        [Display(Name = "Restrykcje")]
        public bool Restriction { get; set; }

        [DataType(DataType.DateTime, ErrorMessage = "Nieprawidłowa data")]
        [Display(Name = "Data rozpoczęcia ")]
        public DateTime StartDate { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        [Display(Name = "Data utworzenia ")]
        public DateTime CreationDate { get; set; }

        [Display(Name = "Kurs")]
        [Required(ErrorMessage = "Podaj id kursu")]
        public int CourseId { get; set; }

        public static TopicViewModel ToTopicViewModel(Topic Topic)
        {
            return new TopicViewModel()
            {
                Name = Topic.Name,
                Description = Topic.Description,
                CreationDate = Topic.CreationDate,
                StartDate = Topic.StartDate.Value,
                Restriction = Topic.Restriction,
                CourseId = Topic.CourseId
            };
        }
    }
}