﻿using NET_Moodle.Domain.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NET_Moodle.Models
{
    public class UserViewModel
    {
        [Display(Name="Login")]
        public string Login { get; set; }
        [Display(Name="Email")]
        [EmailAddress]
        public string Email { get; set; }
        [Display(Name="Imię")]
        public string Name { get; set; }
        [Display(Name="Nazwisko")]
        public string Surname { get; set; }

        public static UserViewModel ToUserViewModel(ApplicationUser User)
        {
            return new UserViewModel()
            {
                Name = User.UserName,
                Surname = User.Surname,
                Email = User.Email
            };
        }
    }
}