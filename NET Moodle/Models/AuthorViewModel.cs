﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NET_Moodle.Models
{
    public class AuthorViewModel
    {
        public string ImageUrl { get; set; }
        public string AltImage { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string EMail { get; set; }
        public string Telephone { get; set; }
        public string Address { get; set; }
        public string Description { get; set; }
        public string Functions { get; set; }
    }
}