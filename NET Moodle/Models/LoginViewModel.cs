﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NET_Moodle.Models
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "Login jest wymagany")]
        [Display(Name = "Nazwa użytkownika")]
        public string LoginName { get; set; }
        [Required(ErrorMessage = "Hasło jest wymagane")]
        [DataType(DataType.Password,ErrorMessage = "Nieprawidłowe hasło")]
        [Display(Name = "Hasło użytkownika")]
        public string Password { get; set; }
        [Display(Name = "Zapamiętaj mnie ?")]
        public bool RememberMe { get; set; }
    }
}