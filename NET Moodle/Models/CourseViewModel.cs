﻿using NET_Moodle.Domain.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NET_Moodle.Models
{
    public class CourseViewModel
    {
        [Display(AutoGenerateField = false)]
        public int Id { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Nazwa", Description = "Nazwa kursu")]
        public string Name { get; set; }

        [Required]
        [DataType(DataType.MultilineText)]
        [Display(Name = "Opis", Description = "Opis kursu")]
        public string Description { get; set; }

        [Required]
        [Display(Name = "Typ", Description = "Typ kursu")]
        public CourseTypeEnum Type { get; set; }

        [Required]
        [Display(Name = "Wymagane hasło? ")]
        public bool IsPasswordEnable { get; set; }

        [DataType(DataType.Password, ErrorMessage = "Nieprawidłowe hasło")]
        [Display(Name = "Hasło ")]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        [Display(Name = "Data utworzenia ")]
        public DateTime CreationDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        [DataType(DataType.DateTime, ErrorMessage = "Nieprawidłowa data")]
        [Display(Name = "Data rozpoczęcia ")]
        public DateTime StartDate { get; set; }

        [Required]
        [Display(Name = "Widoczne? ")]
        public bool IsVisible { get; set; }
        

        [Display(Name = "Skrót ")]
        public string ShortCut { get; set; }


        [Display(Name = "Obrazek ")]
        public Image Image  { get; set; }

        [Required]
        [Display(Name = "Prelegenci")]
        public ICollection<Lecturer> Lecturers { get; set; }

        [Display(Name = "Tagi")]
        public ICollection<Tag> Tags { get; set; }

 
        [Display(Name = "Tematy")]
        public ICollection<Topic> Topics { get; set; }


        public ICollection<ApplicationUser> participants { get; set; }

        public static CourseViewModel ToCourseViewModel(Course Course)
        {
            return new CourseViewModel()
            {
                Id = Course.CourseId,
                Name = Course.Name,
                Description=Course.Description,
                Type=Course.CourseType,
                IsPasswordEnable=Course.IsPasswordEnable,
                Password=Course.Password,
                CreationDate=Course.CreationDate,
                StartDate=Course.StartDate,
                IsVisible=Course.IsVisible,
                ShortCut= Course.ShortCut,
                Image=Course.Image,
                Lecturers=Course.Lecturers,
                Tags=Course.Tags,
                Topics=Course.Topics,
                participants = Course.ApplicationUsers

            };
        }

    }
}