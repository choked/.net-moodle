﻿using NET_Moodle.Helpers;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using TweetSharp;

namespace NET_Moodle.Models
{
    public static class SocialMedia
    {
        private const string AuthenticationUrlFormat = @"https://graph.facebook.com/oauth/access_token?client_id={0}&client_secret={1}&grant_type=client_credentials";
        private const string PageId = "272658349435534"; //fanpage ID goes here, current: put.net

        private static string GetFacebookAccessToken()
        {
            string apiId = "168388670218024";
            string apiSecret = "71978b1831e11ee597efbabf334a0506";

            string url = string.Format(AuthenticationUrlFormat, apiId, apiSecret);
            string tempAccessToken = string.Empty;
            WebRequest request = WebRequest.Create(url);
            try
            {
                WebResponse response = request.GetResponse();

                using (Stream responseStream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                    String responseString = reader.ReadToEnd();

                    NameValueCollection query = HttpUtility.ParseQueryString(responseString);

                    tempAccessToken = query["access_token"];
                }
                if (tempAccessToken.Trim().Length == 0)
                {
                    throw new Exception("Access Token not retrieved successfully.");
                }
            }
            catch (WebException ex)
            {
                if (ex.Message.Contains("remote name could not"))
                {
                    return null;
                }
            }
            
            return tempAccessToken;
        }

        public static IEnumerable<TwitterStatus> GetTweets()
        {
            var service = new TwitterService("iIgMnW0lNIghLT6R5VAtRVFyv", "N9M2dIjK4eTA068TvSZkvqXFRDAjVlq1pHYpiugh2dDE7lI0hW");
            string screenName = "KonradDysput";
            service.AuthenticateWith("720524804030394368-uRxMIYOUXbtJeWZmuRTZeGuV7XLfAGe", "FqywfyqDDC9I32vE3yrOisPw8GZIxf8lReiY89n23buTk");
            var options = new ListTweetsOnUserTimelineOptions()
            {
                ScreenName = screenName,
                Count = 10,
                ExcludeReplies = false
            };

            return service.ListTweetsOnUserTimeline(options);
        }

        public static async Task<FBPostsModel> GetPosts()
        {
            string AccessToken = GetFacebookAccessToken();
            if (AccessToken!=null)
            {
                return await ApiHelper.CallApi<FBPostsModel>(@"https://graph.facebook.com/", $"{PageId}/posts?fields=created_time,message,link,permalink_url,id,admin_creator,likes&access_token={AccessToken}&limit=10", "GET");
            }
            return null;
        }


    }
}