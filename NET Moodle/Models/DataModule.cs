﻿using Autofac;
using NET_Moodle.Domain.Interfaces;
using NET_Moodle.Domain.Model;
using NET_Moodle.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NET_Moodle.Models
{
    public class DataModule : Module
    {
        public DataModule()
        {

        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<MoodleDbContext>()
                .As<IDbContext>().InstancePerRequest()
                .PreserveExistingDefaults();

            builder.RegisterType<CompanyRepository>().As<ICompany>().InstancePerRequest();
            builder.RegisterType<NewsRepository>().As<INews>().InstancePerRequest();
            builder.RegisterType<AuthorRepository>().As<IAuthor>().InstancePerRequest();
            builder.RegisterType<LecturerRepository>().As<ILecturer>().InstancePerRequest();
            builder.RegisterType<CourseRepository>().As<ICourse>().InstancePerRequest();
            builder.RegisterType<UserRepository>().As<IUserRepository>().InstancePerRequest();
            builder.RegisterType<TopicRepository>().As<ITopic>().InstancePerRequest();
            base.Load(builder);
        }
    }
}