﻿using NET_Moodle.Domain.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NET_Moodle.Models
{
    public class LecturerViewModel
    {
        [Required]
        [DataType(DataType.EmailAddress, ErrorMessage = "Podano niepoprawny adres E-Mail")]
        [Display(Name = "Adres E-Mail", Description = "Adres E-Mail wykładowcy")]
        public string EMail { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Imię", Description = "Imię prowadzącego")]
        public string Name { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Nazwisko", Description = "Nazwisko prowadzącego")]
        public string Surname { get; set; }

        [Display(Order = 0, Description =" ",Name =" ")]
        public string ImageAddress { get; set; }


        [Display(Order = 0)]
        public string AlternativeLink { get; set; }

        [Required]
        [DataType(DataType.Url)]
        [Display(Name = "Strona domowa", Description = "Strona domowa prowadzącego")]
        public string HomePageUrl { get; set; }

        public static LecturerViewModel ToLecturerViewModel(Lecturer lecturer)
        {
            return new LecturerViewModel()
            {
                AlternativeLink = lecturer.Image.AlternateString,
                HomePageUrl = lecturer.HomePageAddress,
                EMail = lecturer.Contact.EMail,
                ImageAddress = lecturer.Image.ImagePath,
                Name = lecturer.ApplicationUser.UserName,
                Surname = lecturer.ApplicationUser.Surname
            };
        }
    }
}