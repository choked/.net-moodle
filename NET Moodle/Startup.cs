﻿using Microsoft.Owin;
using Owin;

using NET_Moodle.Controllers;

[assembly: OwinStartupAttribute(typeof(NET_Moodle.Startup))]
namespace NET_Moodle
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
