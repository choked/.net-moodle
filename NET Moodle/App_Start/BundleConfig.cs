﻿using System.Web;
using System.Web.Optimization;

namespace NET_Moodle
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.min.js",
                     "~/Scripts/ripples.min.js",
                     "~/Scripts/material.min.js",
                     "~/Scripts/snackbar.min.js",
                      "~/Scripts/moment.js",
                     "~/Scripts/jquery.nouislider.min.js",
                      "~/Scripts/bootstrap-material-datetimepicker.js",
                     "~/Scripts/scripts.js"
                ));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/Stylesheets/bootstrap.min.css",
                      "~/Content/Stylesheets/bootstrap-material-design.min.css",
                      "~/Content/Stylesheets/ripples.min.css",
                      "~/Content/Stylesheets/snackbar.min.css",
                      "~/Content/Site.min.css",
                     "~/Content/Stylesheets/bootstrap-material-datetimepicker.css"));
        }
    }
}
