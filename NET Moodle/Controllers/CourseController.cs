﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NET_Moodle.Domain.Interfaces;
using NET_Moodle.Domain.Model;
using NET_Moodle.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using NET_Moodle.Domain.Helpers;
using NET_Moodle.Helpers;

namespace NET_Moodle.Controllers
{
    public class CourseController : Controller
    {

        private ICourse _courseRepository;
        private ITopic _topicRepository;
        public CourseController(ICourse courseRepository, ITopic topicRepository)
        {
            _courseRepository = courseRepository;
            _topicRepository = topicRepository;
        }

        [HttpPost]
        public ActionResult ChangeCourseDescription(int courseId, string shortCutEdit, string courseNameEdit, string courseDescriptionEdit)
        {
            _courseRepository.Update(new Course()
            {
                CourseId = courseId,
                ShortCut = shortCutEdit,
                Name = courseNameEdit,
                Description = courseDescriptionEdit
            });
            return RedirectToAction("ManageCourse");
        }

        [HttpPost]
        public ActionResult AcceptPassword(string password, int courseId)
        {
            var course = _courseRepository.GetElementById(courseId);
            bool couseAssigned = true;
            try
            {
                _courseRepository.AssignUserToCourse(courseId, User.Identity.GetUserId(), password);
            }
            catch (Exception ex)
            {
                couseAssigned = false;
            }
            return RedirectToAction("Index", "Course", new { couseAssigned = couseAssigned });
        }

        public ActionResult AcceptCourse(int courseId)
        {
            var course = _courseRepository.GetElementById(courseId);
            if (!course.IsPasswordEnable)
            {
                _courseRepository.AssignUserToCourse(courseId, User.Identity.GetUserId(), string.Empty);
            }
            return Json(new
            {
                accept = course?.IsPasswordEnable
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Index(string searchString, bool? couseAssigned, string sortOrder, int page = 1, int pageSize = 10)
        {
            ViewBag.CouseAssigned = couseAssigned;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";
            ViewBag.StartDateSortParm = sortOrder == "StartDate" ? "start_date_desc" : "StartDate";

            var courses = _courseRepository.Get().ToList();
            if (!String.IsNullOrEmpty(searchString))
            {
                courses = courses.Where(s => s.Description.Contains(searchString) || s.Name.Contains(searchString) || s.ShortCut.Contains(searchString)).ToList();
            }
            var CourseViewModels = courses.Select(n => { return CourseViewModel.ToCourseViewModel(n); }).ToList();
            switch (sortOrder)
            {
                case "name_desc":
                    CourseViewModels = CourseViewModels.OrderByDescending(s => s.Name).ToList();
                    break;
                case "Date":
                    CourseViewModels = CourseViewModels.OrderBy(s => s.CreationDate).ToList();
                    break;
                case "date_desc":
                    CourseViewModels = CourseViewModels.OrderByDescending(s => s.CreationDate).ToList();
                    break;
                case "StartDate":
                    CourseViewModels = CourseViewModels.OrderBy(s => s.StartDate).ToList();
                    break;
                case "start_date_desc":
                    CourseViewModels = CourseViewModels.OrderByDescending(s => s.StartDate).ToList();
                    break;
                default:
                    CourseViewModels = CourseViewModels.OrderBy(s => s.Name).ToList();
                    break;
            }
            PagedList<CourseViewModel> model = new PagedList<CourseViewModel>(CourseViewModels, page, pageSize);
            return View(model);
        }
        [HttpGet]
        [Authorize(Roles = "Admin")]
        public ActionResult AddCourse()
        {
            return View();
        }
        /*[HttpGet]
        public ActionResult GetUserCourses()
        {
            var courses = _courseRepository.GetUserCourses(User.Identity.GetUserId()).Select(n => new
            {
                courseId = n.CourseId,
                courseName = n.Name
            });
            return Json(courses,JsonRequestBehavior.AllowGet);
        }*/
        [HttpPost]
        [Route("Create")]
        [Authorize(Roles = "Admin")]
        public ActionResult AddCourse(CourseViewModel model, HttpPostedFileBase file)
        {
            string targetFolder = Server.MapPath("~/Content/Images");
            string targetPath = Path.Combine(targetFolder, file.FileName);
            file.SaveAs(targetPath);

            Image image = new Image
            {
                AlternateString = file.FileName,
                ImagePath = file.FileName,
            };

            Course course = new Course
            {
                CourseType = CourseTypeEnum.Public,
                CreationDate = DateTime.Now,
                Description = model.Description,
                IsPasswordEnable = model.IsPasswordEnable,
                IsVisible = model.IsVisible,
                Name = model.Name,
                ShortCut = model.ShortCut,
                StartDate = model.StartDate,
                Image = image
            };
            if (model.IsPasswordEnable == true)
            {
                course.Password = model.Password;
            }
            _courseRepository.Add(course);
            return View();
        }

        [Authorize(Roles = "Admin")]
        public ActionResult ManageCourse(string searchString, string sortOrder, int page = 1, int pageSize = 10)
        {
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";
            ViewBag.StartDateSortParm = sortOrder == "StartDate" ? "start_date_desc" : "StartDate";
            var Course = _courseRepository.Get().ToList();
            if (!String.IsNullOrEmpty(searchString))
            {
                Course = Course.Where(s => s.Description.Contains(searchString) || s.Name.Contains(searchString) || s.ShortCut.Contains(searchString)).ToList();
            }
            var CourseViewModels = Course.Select(n => { return CourseViewModel.ToCourseViewModel(n); }).ToList();
            switch (sortOrder)
            {
                case "name_desc":
                    CourseViewModels = CourseViewModels.OrderByDescending(s => s.Name).ToList();
                    break;
                case "Date":
                    CourseViewModels = CourseViewModels.OrderBy(s => s.CreationDate).ToList();
                    break;
                case "date_desc":
                    CourseViewModels = CourseViewModels.OrderByDescending(s => s.CreationDate).ToList();
                    break;
                case "StartDate":
                    CourseViewModels = CourseViewModels.OrderBy(s => s.StartDate).ToList();
                    break;
                case "start_date_desc":
                    CourseViewModels = CourseViewModels.OrderByDescending(s => s.StartDate).ToList();
                    break;
                default:
                    CourseViewModels = CourseViewModels.OrderBy(s => s.Name).ToList();
                    break;
            }
            PagedList<CourseViewModel> model = new PagedList<CourseViewModel>(CourseViewModels, page, pageSize);
            return View(model);
        }

        [HttpPost]
        public ActionResult EditDetails(CourseViewModel viewModel)
        {
            _courseRepository.UpdateDescription(viewModel.ToCourseFromView());
            return RedirectToAction("CourseDetail", "Course", new { id = viewModel.Id });
        }

        [Route("CourseDetail/{id}")]
        public ActionResult CourseDetail(int id)
        {       
            ViewBag.topics = _topicRepository.GetCourseTopics(id);
            var courseUsers = _courseRepository.GetCourseUsers(id);
            ViewBag.Users = courseUsers.Select(n =>
            { return UserViewModel.ToUserViewModel(n); })
                                                    .ToList();
            var CourseViewModels = CourseViewModel.ToCourseViewModel(_courseRepository.GetElementById(id));
            return View(CourseViewModels);
        }

        [Authorize(Roles = "Admin")]
        public ActionResult DeleteCourse(int courseId)
        {
            _courseRepository.DeleteById(courseId);
            return RedirectToAction("ManageCourse");
        }

        [HttpGet]
        public ActionResult GetUserCourses(string id)
        {
            ViewBag.courses = _courseRepository.GetUserCourses(id);
            return PartialView("_UserCoursesPartial");
        }

    }
}