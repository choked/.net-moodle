﻿using NET_Moodle.Domain.Interfaces;
using NET_Moodle.Models;
using System.Linq;
using System.Web.Mvc;

namespace NET_Moodle.Controllers
{
    [RoutePrefix("Lecturer")]
    public class LecturerController : Controller
    {
        private ILecturer lecturerRepository;

        public LecturerController(ILecturer lecturerRepository)
        {
            this.lecturerRepository = lecturerRepository;
        }

        // GET: Lecturer
        [Route("")]
        [HttpGet]
        public ActionResult Index()
        {
            var lecturers = lecturerRepository.Get().ToList();
            var lecturerViewModels = lecturers.Select(n => { return LecturerViewModel.ToLecturerViewModel(n); });
            return View(lecturerViewModels);
        }

        [Authorize(Roles = "Admin")]
        public ActionResult Manage()
        {
            var lecturers = lecturerRepository.Get().ToList();
            var lecturerViewModels = lecturers.Select(n => { return LecturerViewModel.ToLecturerViewModel(n); });
            return View(lecturerViewModels);
        }
    }
}