﻿using NET_Moodle.Domain.Interfaces;
using NET_Moodle.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NET_Moodle.Controllers
{
    [RoutePrefix("Authors")]
    public class AuthorController : Controller
    {
        private IAuthor authorRepository;
        public AuthorController(IAuthor authorRepository)
        {
            this.authorRepository = authorRepository;
        }
        // GET: Author
        [Route("")]
        public ActionResult Index()
        {
            var authors = authorRepository.Get().ToAuthorViewModel();
            return View(authors);
        }
    }
}