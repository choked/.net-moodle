﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NET_Moodle.Domain.Interfaces;
using NET_Moodle.Domain.Model;
using NET_Moodle.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace NET_Moodle.Controllers
{
    public class HomeController : AsyncController
    {

        private INews newsRepository;
        public HomeController(INews newsRepository)
        {
            this.newsRepository = newsRepository;
        }
        public async Task<ActionResult> Index()
        {
            var News = newsRepository.Get().Reverse().ToList();
            var NewsViewModels = News.Select(n => { return NewsViewModel.ToNewsViewModel(n); }).ToList();

            ViewBag.TwitterPosts = SocialMedia.GetTweets();
            ViewBag.FacebookPosts = await SocialMedia.GetPosts();

            return View(NewsViewModels);
        }
        public ActionResult UserView()
        {
            return View("UserView");
        }
    }
}