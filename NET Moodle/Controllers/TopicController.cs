﻿using NET_Moodle.Domain.Interfaces;
using NET_Moodle.Domain.Model;
using NET_Moodle.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NET_Moodle.Controllers
{
    public class TopicController : Controller
    {

        private ICourse _courseRepository;
        private ITopic _topicRepository;
        public TopicController(ITopic topicRepository, ICourse courseRepository)
        {
            _topicRepository = topicRepository;
            _courseRepository = courseRepository;
        }

        [HttpGet]
        public ActionResult AddTopic()
        {
            return View();
        }

        [HttpPost]
        [Route("Create")]
        public ActionResult AddTopic(TopicViewModel model, int id)
        {
            Topic topic = new Topic
            {
                Name = model.Name,
                Description = model.Description,            
                StartDate = model.StartDate,
                CreationDate = DateTime.Now,
                Restriction = model.Restriction,
                CourseId = id
            };

            _topicRepository.Add(topic);
            return RedirectToAction("CourseDetail","Course",new { id = id });
        }
    }
}