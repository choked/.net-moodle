﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NET_Moodle.Domain.Interfaces;
using NET_Moodle.Domain.Model;
using NET_Moodle.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NET_Moodle.Controllers
{
    public class NewsController : Controller
    {
        private INews _newsRepository;
        private ILecturer _lecturerRepository;
        public NewsController(INews newsRepository, ILecturer lecturerRepository)
        {
            _newsRepository = newsRepository;
            _lecturerRepository = lecturerRepository;
        }

        public ActionResult Index()
        {
            return View();
        }
        // GET: News
        [Authorize(Roles ="Admin")]
        public ActionResult ManageNews()
        {
            var News = _newsRepository.Get().ToList();
            var NewsViewModels = News.Select(n => { return NewsViewModel.ToNewsViewModel(n); }).ToList();
            return View(NewsViewModels);
        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        public ActionResult CreateNews()
        {
            return View();
        }
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult CreateNews(NewsViewModel newsViewModel, HttpPostedFileBase file)
        {

            //newsViewModel.LecturerId = _lecturerRepository.GetByUserId(userId).LecturerId;
            if (!ModelState.IsValid || file == null)
            {
                return View(newsViewModel);
            }
            string targetFolder = Server.MapPath("~/Content/Images");
            string targetPath = Path.Combine(targetFolder, file.FileName);
            file.SaveAs(targetPath);

            Image image = new Image
            {
                AlternateString = file.FileName,
                ImagePath = file.FileName,
            };
            var userId = User.Identity.GetUserId();
            News news = new News()
            {
                CreationDate = DateTime.Now,
                LecturerId = _lecturerRepository.GetByUserId(userId).LecturerId,
                Description = newsViewModel.Description,
                Image = image,
                PhotoName = file.FileName,
                TopicName = newsViewModel.Title

            };

            _newsRepository.Add(news);


            return RedirectToAction("ManageNews");
        }

        [Authorize(Roles = "Admin")]
        public ActionResult DeleteNews(int newsId)
        {
            _newsRepository.DeleteById(newsId);
            return RedirectToAction("ManageNews");
        }



    }
}