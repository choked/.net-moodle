﻿using NET_Moodle.Domain.Interfaces;
using NET_Moodle.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NET_Moodle.Controllers
{
    public class CompanyController : Controller
    {
        private ICompany company;
        public CompanyController(ICompany company)
        {
            this.company = company;
        }
        // GET: Company
        public ActionResult Index()
        {
            return View(company.Get().ToCompanyViewModel());
        }
    }
}