﻿using NET_Moodle.Domain.Interfaces;
using NET_Moodle.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NET_Moodle.Controllers
{
    [RoutePrefix("UserPanel")]
    public class UserPanelController : Controller
    {
        private IUserRepository _userRepository;

        public UserPanelController(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }
        [Route("Users")]
        [Authorize(Roles = "Admin")]
        public ActionResult GetUsers()
        {
            var users = _userRepository.GetUsers();
            return View(users.Select(n=>new UserViewModel()
            {
                Email=n.Email,
                Name=n.UserName,
                Surname = n.Surname
            }));
        }
        // GET: UserPanel
        [Route("")]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CheckData(LoginViewModel check)
        {
            if (!ModelState.IsValid)
                return View("Index", check);
            else
            {
                return View("Index");
            }
        }
    }
}