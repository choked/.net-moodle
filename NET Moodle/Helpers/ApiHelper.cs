﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace NET_Moodle.Helpers
{
    internal static class ApiHelper
    {
        public static async Task<T> CallApi<T>(string hostUrl, string requestUri = "", string method = "GET", object obj = null)
        {
            using (HttpClient client = new HttpClient())
            {

                client.BaseAddress = new Uri(hostUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response;

                if (method == "GET")
                {
                    response = await client.GetAsync(requestUri);
                }
                else if (method == "PUT" || method == "POST")
                {
                    HttpContent content;

                    var sendJson = JsonConvert.SerializeObject(obj).ToString();

                    content = new StringContent(sendJson, Encoding.UTF8, "application/json");

                    if (method == "PUT")
                    {
                        response = await client.PutAsync(requestUri, content);
                    }
                    else
                    {
                        response = await client.PostAsync(requestUri, content);
                    }
                }
                else
                {
                    response = new HttpResponseMessage();
                }


                if (!response.IsSuccessStatusCode)
                {
                    throw (new HttpException((int)response.StatusCode, requestUri));
                }

                var json = await response.Content.ReadAsStringAsync();
                var result = JsonConvert.DeserializeObject<T>(json);
                return result;
            }
        }

        public static async Task CallApi(string hostUrl, string requestUri = "", string method = "GET", object obj = null)
        {

            using (HttpClient client = new HttpClient())
            {

                client.BaseAddress = new Uri(hostUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response;
                if (method == "GET")
                {
                    response = await client.GetAsync(requestUri);
                }
                else if (method == "PUT" || method == "POST")
                {
                    HttpContent content;

                    var sendJson = JsonConvert.SerializeObject(obj).ToString();

                    content = new StringContent(sendJson, Encoding.UTF8, "application/json");

                    if (method == "PUT")
                    {
                        response = await client.PutAsync(requestUri, content);
                    }
                    else
                    {
                        response = await client.PostAsync(requestUri, content);
                    }
                }
                else
                {
                    response = new HttpResponseMessage();
                }

                if (!response.IsSuccessStatusCode)
                {
                    throw (new HttpException((int)response.StatusCode, requestUri));
                }

            }
        }
    }
}