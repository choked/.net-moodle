﻿using NET_Moodle.Domain.Model;
using NET_Moodle.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NET_Moodle.Helpers
{
    public static class ViewModelHelper
    {
        public static IEnumerable<AuthorViewModel> ToAuthorViewModel(this IEnumerable<Author> authors)
        {
            return authors.Select(n => new AuthorViewModel()
            {
                Address = n.Contact.Address ?? string.Empty + ' ' + n.Contact.Town + ' ' + n.Contact.Country,
                AltImage = n.Image.AlternateString,
                ImageUrl = n.Image.ImagePath,
                Description = n.Description,
                EMail = n.Contact.EMail,
                Functions = n.Functions,
                Name = n.Name,
                Surname = n.Surname,
                Telephone = n.Contact.Telephone
            }
            );
        }
        public static IEnumerable<CompanyViewModel> ToCompanyViewModel(this IEnumerable<Company> companies)
        {
            return companies.Select(n => new CompanyViewModel()
            {
                Address = n.Contact.Address ?? string.Empty + ' ' + n.Contact.Town + ' ' + n.Contact.Country,
                AltImage = n.Image.AlternateString,
                ImageUrl = n.Image.ImagePath,
                Description = n.CompanyDescription,
                EMail = n.Contact.EMail,
                Telephone = n.Contact.Telephone,
                Name = n.CompanyName
            }
            );
        }

        public static Course ToCourseFromView(this CourseViewModel viewModel)
        {
            return new Course()
            {
                CourseId = viewModel.Id,
                CourseType = viewModel.Type,
                IsVisible = viewModel.IsVisible,
                IsPasswordEnable = viewModel.IsPasswordEnable,
                ShortCut = viewModel.ShortCut,
                Password = viewModel.Password
            };
        }
    }
}